//
//  ResultViewController.swift
//  QuizApp
//
//  Created by Alima Aglakova on 29/01/2021.
//

import UIKit

class ResultViewController: UIViewController {
    
    var delegate: QuizViewControllerDelegate?
    var mainDelegate: ViewControllerDelegate?

    
    @IBOutlet weak var scoreLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        updateData()
    }
    
    func updateData() {
        scoreLabel.text = "You scored \(delegate?.getScore() ?? 0)/10"
        mainDelegate?.setScore((delegate?.getScore())!)
    }
    
    @IBAction func tryAgainButton(_ sender: Any) {
        delegate?.refreshData()
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func mainPageButton(_ sender: Any) {
        navigationController?.popToRootViewController(animated: true)
    }
    
}
