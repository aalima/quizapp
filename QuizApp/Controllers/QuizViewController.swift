//
//  QuizViewController.swift
//  QuizApp
//
//  Created by Alima Aglakova on 27.01.2021.
//

import UIKit

class QuizViewController: UIViewController {
    var delegate: ViewControllerDelegate?
    var questionId = 0
    var score = 0
    var questions: [Question] = []

    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn4: UIButton!
    @IBOutlet weak var prevButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        questions = (delegate?.getQuiz())!
        refreshData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        quizQuestions()
    }

    func quizQuestions() {
        if questionId == 0 {
            prevButton.isHidden = true;
        } else {
            prevButton.isHidden = false;
        }
        if questionId == questions.count-1 {
            nextButton.setTitle("Submit", for: .normal)
        } else {
            nextButton.setTitle("Next", for: .normal)
        }
        let question = questions[questionId]
        questionLabel.text = question.question
        btn1.setTitle(question.answer[0], for: .normal)
        btn2.setTitle(question.answer[1], for: .normal)
        btn3.setTitle(question.answer[2], for: .normal)
        btn4.setTitle(question.answer[3], for: .normal)

        btn1.backgroundColor = .link
        btn2.backgroundColor = .link
        btn3.backgroundColor = .link
        btn4.backgroundColor = .link
        
        if question.chosen != -1 {
            btn1.isEnabled = false;
            btn2.isEnabled = false;
            btn3.isEnabled = false;
            btn4.isEnabled = false;
            var btn = btn1
            var corBtn = btn1
            switch question.correctAnswer {
            case 0:
                corBtn = btn1
            case 1:
                corBtn = btn2
            case 2:
                corBtn = btn3
            case 3:
                corBtn = btn4
            case .none: break
            case .some(_): break
            }
            switch question.chosen {
            case 0:
                btn = btn1
            case 1:
                btn = btn2
            case 2:
                btn = btn3
            case 3:
                btn = btn4
            case .none: break
            case .some(_): break
            }
            if question.chosen == question.correctAnswer {
                btn?.backgroundColor = .systemGreen
                score += 1
            } else {
                corBtn?.backgroundColor = .systemGreen
                btn?.backgroundColor = .systemRed
            }
        } else {
            btn1.isEnabled = true;
            btn2.isEnabled = true;
            btn3.isEnabled = true;
            btn4.isEnabled = true;
        }
    }
    
    @IBAction func prevIsPressed(_ sender: Any) {
        questionId -= 1
        quizQuestions()
    }
    
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        if questionId == questions.count-1 {
            let vc = storyboard?.instantiateViewController(identifier: "ResultViewController") as! ResultViewController
            vc.delegate = self
            vc.mainDelegate = delegate
            navigationController?.pushViewController(vc, animated: true)
            return
        }
        questionId += 1
        quizQuestions()
    }
    
    @IBAction func answerIsPressed(_ sender: UIButton) {
        switch sender.tag {
        case 1:
            questions[questionId].chosen=0
        case 2:
            questions[questionId].chosen=1
        case 3:
            questions[questionId].chosen=2
        case 4:
            questions[questionId].chosen=3
        default:
            break
        }
        quizQuestions()
        
    }
    
}

extension QuizViewController: QuizViewControllerDelegate {
    func refreshData() {
        questionId = 0
        score = 0
        for i in 0..<questions.count {
            questions[i].chosen = -1
        }
    }
    
    func getScore() -> Int {
        return score
    }
}

protocol QuizViewControllerDelegate {
    func getScore()->Int
    func refreshData()
}
