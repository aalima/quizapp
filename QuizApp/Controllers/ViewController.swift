//
//  ViewController.swift
//  QuizApp
//
//  Created by Alima Aglakova on 27.01.2021.
//

import UIKit

protocol ViewControllerDelegate {
    func getQuiz()->([Question])
    func setScore(_ score: Int)
    func getHistory()->[Score]
}

class ViewController: UIViewController {
    var quiz = [Question]()
    var history = [Score]()

    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var historyButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        quizQuestions()
    }

    
    func quizQuestions() {
        quiz.append(Question(id: 1, question: "What is my name?", answer: ["Amina","Alima","Aiym","Aizhan"], correctAnswer: 1, chosen: -1))
        quiz.append(Question(id: 2, question: "In which group am I?", answer: ["SE1908","SE1901","CS1903","SE1902"], correctAnswer: 3, chosen: -1))
        quiz.append(Question(id: 3, question: "What is my educational program?", answer: ["Computer Science","Cybersecurity","Software Engineering","Telecommunication systems"], correctAnswer: 2, chosen: -1))
        quiz.append(Question(id: 4, question: "Where was I born?", answer: ["Astana","Semey","Almaty","Karaganda"], correctAnswer: 0, chosen: -1))
        quiz.append(Question(id: 5, question: "Which school did I graduate?", answer: ["Nur Orda","KTL","Lyceum 15","NIS IB"], correctAnswer: 3, chosen: -1))
        quiz.append(Question(id: 6, question: "What would I choose?", answer: ["Android","iOS","Linux","Windows"], correctAnswer: 1, chosen: -1))
        quiz.append(Question(id: 7, question: "Where did I study before?", answer: ["London","Florida","Paris","Istambul"], correctAnswer: 0, chosen: -1))
        quiz.append(Question(id: 8, question: "What is my favourite colour?", answer: ["Red","Green","Black","Blue"], correctAnswer: 2, chosen: -1))
        quiz.append(Question(id: 9, question: "How many students are in my group?", answer: ["20","19","18","17"], correctAnswer: 2, chosen: -1))
        quiz.append(Question(id: 10, question: "In what language is iOS written in?", answer: ["Swift","Java","Golang","C++"], correctAnswer: 0, chosen: -1))
    }
    
    @IBAction func startClicked(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: "QuizViewController") as! QuizViewController
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func historyClicked(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: "HistoryViewController") as! HistoryViewController
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension ViewController: ViewControllerDelegate {
    func getHistory() -> [Score] {
        return history
    }
    
    func setScore(_ score: Int) {
        history.append(Score(title: "Attempt \(history.count)", points: score, noOfQuest: quiz.count))
    }
    
    func getQuiz() -> ([Question]) {
        return quiz
    }
}

