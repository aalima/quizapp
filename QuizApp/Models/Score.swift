//
//  Score.swift
//  QuizApp
//
//  Created by Alima Aglakova on 28.01.2021.
//
import Foundation
public struct Score {
    let title: String?
    let points: Int?
    let noOfQuest: Int?
}
