//
//  Question.swift
//  QuizApp
//
//  Created by Alima Aglakova on 28.01.2021.
//
import Foundation

public struct Question {
    let id: Int?
    let question: String?
    let answer: [String?]
    let correctAnswer: Int?
    var chosen: Int?
}
